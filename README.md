# Installation
> `npm install --save @types/passport`

# Summary
This package contains type definitions for Passport (http://passportjs.org).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/passport

Additional Details
 * Last updated: Tue, 25 Dec 2018 05:42:14 GMT
 * Dependencies: @types/express
 * Global values: none

# Credits
These definitions were written by Horiuchi_H <https://github.com/horiuchi>, Eric Naeseth <https://github.com/enaeseth>, Igor Belagorudsky <https://github.com/theigor>, Tomek Łaziuk <https://github.com/tlaziuk>, Daniel Perez Alvarez <https://github.com/danielpa9708>, Kevin Stiehl <https://github.com/kstiehl>.
